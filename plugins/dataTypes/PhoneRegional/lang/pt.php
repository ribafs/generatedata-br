<?php

$L = array();

$L["DATA_TYPE"] = array(
    "NAME" => "Pone / Fax, Regional",
    "DESC" => "Geerar um número de telefone no formato apropriado para o registro. If it encounters an unfamiliar country, it generates a default phone number in the format (xxx) xxx-xxxx."
);
