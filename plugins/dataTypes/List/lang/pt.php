<?php

$L = array();

$L["DATA_TYPE"] = array(
    "NAME" => "Lista Customizada",
    "DESC" => "Generates one or more random values from an arbitrary list of items."
);

$L["at_most"] = "At Most";
$L["colours"] = "red|orange|yellow|green|blue|indigo|violet";
$L["companies"] = "Microsoft|Macromedia|Google|Yahoo|Lycos|Altavista|Chami|Adobe|Borland|Lavasoft|Cakewalk|Sibelius|Finale|Apple Systems";
$L["company_names"] = "Accounting|Advertising|Asset Management|Customer Relations|Customer Service|Finances|Human Resources|Legal Department|Media Relations|Payroll|Public Relations|Quality Assurance|Sales and Marketing|Research and Development|Tech Support";
$L["exactly"] = "Exactly";
$L["example_1"] = "Números pares menores que 50";
$L["example_2"] = "Números ímpares menores que 50";
$L["example_3"] = "One-Ten";
$L["example_4"] = "Números Primes menores que 100";
$L["example_5"] = "Cores";
$L["example_6"] = "Marital Status";
$L["example_7"] = "Title";
$L["example_8"] = "Department Names";
$L["example_9"] = "Company Names";
$L["example_10"] = "Drug Names";
$L["example_11"] = "Tipos de Alimentos";
$L["example_12"] = "Marcas de Carros";
$L["help"] = "Enter a list of items, separated by a pipe | character. Then select whether you want <b>Exactly</b> X number of items, or <b>At most</b> X items from the list. Multiple items are returned in a comma-delimited list in the results. If you want your data set to include empty values, just add one or more pipe characters at the end - the more pipes you enter, the greater the probability of an empty value being generated.";
$L["one_to_ten"] = "um|dois|três|quatro|cinco|seis|sete|oito|nove|dez";
$L["prefix"] = "Dr.|Sr.|Sra.|Srta.|";
$L["relationship_states"] = "Solteiro|Casado|Divorciado|Common-Law";
$L["separated_by_pipe"] = "Entre os valores separadospor |";
$L["incomplete_fields"] = "The Custom List Data Type needs to have the format entered in the Options text field. Please fix the following rows:";
$L["invalid_int_fields"] = "Please only enter numbers in the \"Exactly\" and \"At Most\" fields for the Custom List Data Type. Please fix the following rows:";
$L["drug_names"] = "Hydrocodone/APAP|Hydrocodone/APAP|LevothyroxineSodium|Lisinopril|Lipitor|Simvastatin|Plavix|Singulair|Azithromycin|Crestor|Nexium|LevothyroxineSodium|Metoprolol Tartrate |Hydrocodone/APAP|Synthroid|Lexapro|Proair HFA|Ibuprofen (Rx)|Trazodone HCl|Amoxicillin|Omeprazole (Rx)|Advair Diskus|Cymbalta|Azithromycin|Tramadol HCl|Omeprazole (Rx)|Amoxicillin|Sulfamethoxazole/Trimethoprim|Amlodipine Besylate|Fluticasone Propionate|Hydrochlorothiazide|Hydrochlorothiazide|Ventolin HFA|Diovan|Warfarin Sodium|Sertraline HCl|Alprazolam|Pravastatin Sodium|Simvastatin|Metoprolol Succinate|Lisinopril/Hydrochlorothiazide|Seroquel|Promethazine HCl|Metoprolol Succinate|Amoxicillin Trihydrate/Potassium Clavulanate|Simvastatin|Amlodipine Besylate|Oxycodone/APAP|Warfarin Sodium|Zolpidem Tartrate|Metformin HCl|Prednisone|Metformin HCl|Fluconazole|Clonazepam|Omeprazole (Rx)|Diovan HCT|Simvastatin|Actos|Sertraline HCl|Gabapentin|Lantus|Amlodipine Besylate|Vitamin D (Rx)|Fluoxetine HCl|Furosemide|Citalopram HBr|Alprazolam|Ciprofloxacin HCl|Atenolol|Meloxicam|Lorazepam|Prednisone|Celebrex|Venlafaxine HCl ER|Lisinopril|Lyrica|Nasonex|Spiriva Handihaler|Furosemide|Cyclobenzaprin HCl|Cephalexin|Abilify|Citalopram HBR|Cyclobenzaprin HCl|Carvedilol|Metformin HCl|Potassium Chloride|Viagra|Amlodipine Besylate|Furosemide|Ibuprofen (Rx)|Azithromycin|Vyvanse|Zetia|Albuterol|Zolpidem Tartrate|Tramadol HCl|Prednisone|Simvastatin|Clonazepam|Namenda|Januvia|Tricor|Diazepam|Simvastatin|Lisinopril|Lisinopril|Loestrin 24 Fe|Metformin HCl|Furosemide|Prednisone|Allopurinol|Amoxicillin Trihydrate/Potassium Clavulanate|Cialis|Alendronate Sodium|Losartan Potassium|Triamterene/Hydrochlorothiazide|Sertraline HCl|Amitriptyline HCl|Oxycodone HCl|Alprazolam|Alprazolam|Fluticasone Propionate|Fluticasone Propionate|Simvastatin|Triamterene/Hydrochlorothiazide|Suboxone|Oxycontin|Niaspan|Lorazepam|Doxycycline Hyclate|Amoxicillin|Digoxin|Potassium Chloride|Vytorin|Lantus Solostar|APAP/Codeine|Hydrocodone/APAP|Triamcinolone Acetonide|Atenolol|Premarin|Paroxetine HCl|Metoprolol Succinatee|Fluoxetine HCl|Flovent HFA|Promethazine HCl|Carisoprodol|Klor-Con M20|Lorazepam|Benicar|Lovastatin|Lisinopril|Amphetamine Salts|Ciprofloxacin HCl|Carvedilol|Risperidone|Levoxyl|Atenolol|Naproxen|Lovaza|Lisinopril|Alprazolam|Atenolol|Bystolic|Penicillin VK|Famotidine|Methylprednisolone|Gabapentin|Nuvaring|Alendronate Sodium|Amoxicillin|Cephalexin|Glyburide|Clindamycin HCl|Folic Acid|Enalapril Maleate|Tamsulosin HCl|Cheratussin AC|Prednisone|Benicar HCT|Gabapentin|Levaquin|TriNessa|Ranitidine HCl|Levothyroxine Sodium|Glipizide|Doxycycline Hyclate|Tri-Sprintec|Amlodipine Besylate|Symbicort|Lidoderm|Pantoprazole Sodium|Pantoprazole Sodium|Zyprexa|Endocet|Zolpidem Tartrate|Gianvi|Lisinopril|Diazepam";
$L["food_types"] = "pasta|pies|salads|sandwiches|soups|stews|cereals|seafood|desserts|noodles";
$L["car_brands"] = "Toyota|Volkswagen|BMW|Mercedes-Benz|Ford|Nissan|Honda|Porsche|Hyundai Motors|Renault|Peugeot|Chevrolet|Fiat|Audi|Kia Motors|Suzuki|General Motors|Citroën|Daimler|GMC|JLR|Ferrari|Tata Motors|Mazda|Subaru|Lexus|Volvo|MINI|Vauxhall|Isuzu|Dongfeng Motor|Mitsubishi Motors|Mahindra and Mahindra|Daihatsu|Jeep|Kenworth|Dodge|Cadillac|Chrysler|Skoda|Acura|Infiniti|Buick|Lincoln|RAM Trucks|Maruti Suzuki|Smart|Dacia|FAW|Seat";
