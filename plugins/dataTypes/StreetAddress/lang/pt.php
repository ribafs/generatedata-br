<?php

$L = array();
$L["DATA_TYPE"] = array(
    "NAME" => "Logradouro",
    "DESC" => "Gerar um tipo de logradouro aleatório."
);

$L["ap_num"] = "Ap #";
$L["po_box"] = "P.O. Box";
$L["street_types"] = "Tv., Travessa,R,Rua,V., Vila,Av.,Avenida";
