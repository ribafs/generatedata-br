<?php

$L = array();

$L["DATA_TYPE"] = array(
    "NAME" => "Faixa de números",
    "DESC" => "This randomly generates a number between the values you specify. Both fields allow you to enter negative numbers."
);

$L["and"] = "e";
$L["between"] = "Entre";
$L["incomplete_fields"] = "Favor entrar a faixa de números (menor e maior) para os seguintes registros: ";
