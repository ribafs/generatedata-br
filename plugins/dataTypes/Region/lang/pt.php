<?php

$L = array();
$L["DATA_TYPE"] = array(
    "NAME" => "Região",
    "DESC" => "Generates random Canadian provinces, states, territories or counties, based on the options you select."
);

$L["help_text"] = "The <b>Full Name</b> and <b>Abbreviation</b> sub-options determine whether the output will contain the full string (e.g. &quot;British Columbia&quot;) or its abbreviation (e.g. &quot;BC&quot;). For UK counties, the abbreviation is the standard 3-character Chapman code.";
$L["full"] = "Completo";
$L["short"] = "Reduzido";
$L["no_countries_selected"] = "Nenhum país selecionado.";
