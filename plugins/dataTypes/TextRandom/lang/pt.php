<?php

$L = array();
$L["DATA_TYPE"] = array(
    "NAME" => "Número aleatório de palavras",
    "DESC" => "This option generates a random number of words - the total number within the range that you specify (inclusive)."
);

$L["incomplete_fields"] = "Please enter the min and max number of words you want to generate for all Random Number of Words fields. See rows: ";
$L["start_with_lipsum"] = "Iniciar com \"Lorem Ipsum...\"";
$L["generate"] = "Gerar";
$L["to"] = "para";
$L["words"] = "palavras";
$L["help"] = "This option generates a random number of words - the total number within the range that you specify (inclusive). As with the Fixed number option, the words are pulled the standard lorem ipsum latin text.";
