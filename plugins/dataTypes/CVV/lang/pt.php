<?php

$L = array();

$L["DATA_TYPE"] = array(
    "NAME" => "Número do CVV do Cartão de crédito",
    "DESC" => "Generates a random credit card CVV number from 111 to 999."
);
