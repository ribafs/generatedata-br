<?php

$L = array();

$L["DATA_TYPE"] = array(
    "NAME" => "Moeda",
    "DESC" => "Gerar um valor de moeda aleatório no formato e faixa que você necessitar."
);

$L["help_intro"] = "This data type generates random currency values, in whatever format and range you want. The example dropdown contains several options so you can get a sense of how it works, but here's what each of the options means.";
$L["format"] = "Formato";
$L["format_desc"] = "The format field governs exactly how the money value should be formatted. <b>X</b>'s are converted into a number: all other values are left as-is.";
$L["range_from"] = "Faixa - De";
$L["range_from_desc"] = "Specifies the lower range of whatever values you want to generate. Note: this field should only contain numbers and (if you want) a decimal point and two following numbers to represent cents/pence/etc.";
$L["range_to"] = "Faixa - Para";
$L["range_to_desc"] = "The upper range of the numbers to generate.";
$L["currency_symbol"] = "Símbolo da Moeda";
$L["currency_symbol_desc"] = "Whatever currency symbol you want to use, e.g. <b>$</b>, <b>€</b>, etc.";
$L["prefix_suffix"] = "Prefixo/Sufixo";
$L["prefix_suffix_desc"] = "This determines where the currency symbol should appear.";

$L["no_cents"] = "sem centavos";
$L["no_thousand_delimiters"] = "sem delimitador de milhares";
$L["no_dollar_sign"] = "sem sinal de dólar";
$L["range"] = "Faixa";
$L["to"] = "Para";
$L["currency_symbol"] = "Símbolo da moeda";
$L["prefix"] = "prefixo";
$L["suffix"] = "sufixo";
$L["incomplete_fields"] = "The Currency data type needs to have the format entered. Please fix the following rows:";
$L["invalid_range_fields"] = "Please only enter numbers in the Currency range fields (0-9 and decimal point). Please fix the following rows: ";
$L["invalid_range"] = "Please check the range goes from small to large. Please fix the following rows: ";
