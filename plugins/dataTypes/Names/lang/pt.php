<?php

$L = array();

$L["DATA_TYPE"] = array(
    "NAME" => "Nomes",
    "DESC" => "Gerar nomes humanos em qualquer formato."
);

$L["help_intro"] = "You can specify multiple formats by separating them with the pipe (|) character. The following strings will be converted to their random name equivalent:";
$L["incomplete_fields"] = "The Name data type needs to have the format entered in the Options text field. Please fix the following rows:";
$L["name"] = "Name";
$L["example_FemaleName"] = "Jane (Nome de mulhar)";
$L["example_FemaleName_Surname"] = "Jane Abreu";
$L["example_MaleName"] = "João (Nome de homem)";
$L["example_MaleName_Surname"] = "Abreu Cunha";
$L["example_Name"] = "Alex (quanquer gênero)";
$L["example_Name4"] = "Jeni, Tobias, Bento, Pedro";
$L["example_Name_Initial_Surname"] = "Alex J. Santos";
$L["example_Name_Surname"] = "Alex Smith";
$L["example_Surname_Name_Initial"] = "Smith, John P.";
$L["example_fullnames"] = "Alex Smith or Alex J. Smith";
$L["example_surname"] = "Sousa (sobrenome)";
$L["type_FemaleName"] = "Nome de mulher, primeiro nome.";
$L["type_Initial"] = "Uma letra maiúscula, A-Z.";
$L["type_MaleName"] = "Uma letra inicial de homem.";
$L["type_Name"] = "A first name, male or female.";
$L["type_Surname"] = "Um sobrenome aleatório.";
