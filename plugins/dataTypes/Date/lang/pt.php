<?php

$L = array();

$L["DATA_TYPE"] = array(
    "NAME" => "Data",
    "DESC" => "Generates a custom formatted date/datetime/time between whatever two dates you specify."
);

$L["char"] = "Caractere";
$L["day"] = "Dia";
$L["description"] = "Description";
$L["example"] = "Example";
$L["format_code"] = "Format code:";
$L["from"] = "De:";
$L["help_D"] = "A textual representation of a day, three letters";
$L["help_D_example"] = "Domingo a Sábado";
$L["help_F"] = "A full textual representation of a month, such as January or March";
$L["help_F_example"] = "Janeiro a Dezembro";
$L["help_L"] = "Whether it&#39;s um ano bissexto";
$L["help_L_example"] = "1 se for um bisexto, 0 se não for.";
$L["help_M"] = "A short textual representation of a month, three letters";
$L["help_M_example"] = "Jan a Dez";
$L["help_S"] = "English ordinal suffix for the day of the month, 2 characters st, nd, rd or th. Works well with j";
$L["help_S_example"] = "Domingo a Sábado";
$L["help_W"] = "ISO-8601 week number of year, weeks starting on Monday";
$L["help_W_example"] = "42 (the 42nd week in the year)";
$L["help_Y"] = "A full numeric representation of a year, 4 digits";
$L["help_Y_example"] = "1999 or 2016";
$L["help_d"] = "Day of the month, 2 digits with leading zeros";
$L["help_d_example"] = "01 a 31";
$L["help_intro"] = "This data type randomly generates a date between the dates specified, and allows for unique formatting of the result. See the table below for a list of viable formatting rules (these are cribbed from the standard PHP date() formatting options). Take a look at the example dropdown to select from existing formatting options.";
$L["help_j"] = "Day of the month without leading zeros";
$L["help_j_example"] = "1 a 31";
$L["help_l"] = "A full textual representation of the day of the week";
$L["help_l_example"] = "Domingo a Sábado";
$L["help_m"] = "Numeric representation of a month, with leading zeros";
$L["help_m_example"] = "01 a 12";
$L["help_n"] = "Numeric representation of a month, without leading zeros";
$L["help_n_example"] = "1 a 12";
$L["help_t"] = "Number of days in the given month";
$L["help_t_example"] = "28 a 31";
$L["help_w"] = "Numeric representation of the day of the week";
$L["help_w_example"] = "0 (para Domingo) a 6 (para Sábado)";
$L["help_y"] = "A two digit representation of a year";
$L["help_y_example"] = "99 ou 03";
$L["help_z"] = "The day of the year (starting from 0)";
$L["help_z_example"] = "0 a 365";
$L["incomplete_fields"] = "Dates needs to have the format entered in the Options text field. Please fix the following rows:";
$L["month"] = "Mês";
$L["to"] = "Para:";
$L["week"] = "Semana";
$L["year"] = "Ano";
