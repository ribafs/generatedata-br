<?php

$L = array();
$L["DATA_TYPE"] = array(
    "NAME" => "Número Fixo de Palavras",
    "DESC" => "Gerar um número fixo de palavras aleatórias, preenchidas com o padrão lorem ipsum latin."
);

$L["TextFixed_generate"] = "Gerar";
$L["TextFixed_help"] = "This option generates a fixed number of random words, pulled from the standard <a href=&quot;http://en.wikipedia.org/wiki/Lorem_ipsum&quot; target=&quot;_blank&quot;>lorem ipsum</a> latin text.";
$L["TextFixed_words"] = "palavras";
$L["incomplete_fields"] = "Favor entrar o número de palavras que você deseja gerar. Veja os regsitros: ";
