<?php

$L = array();
$L["row_label"] = "Campo da Tabela";
$L["row_label_plural"] = "Campos da Table";
$L["validation_invalid_col_name"] = "Please enter valid database column names for each row (a-Z and underscore characters). Please fix the following rows: ";
$L["validation_invalid_table_name"] = "Please enter a valid database table name (a-Z and underscores only).";
$L["validation_invalid_batch_size"] = "Please enter valid batch size (1 - 300)";

$L["db_table_name"] = "Nome da Tabela";
$L["db_type"] = "Tipo da Tabela";
$L["misc_options"] = "Misc Options";
$L["include_create_table_query"] = "Include CREATE TABLE query";
$L["include_drop_table_query"] = "Include DROP TABLE query";
$L["enclose_table_backquotes"] = "Enclose table and field names with backquotes";
$L["statement_type"] = "Statement Type";
$L["primary_key"] = "Primary Key";
$L["add_default_auto_increment_col"] = "Add default auto-increment column";
$L["insert_batch_size"] = "INSERT batch size";
$L["batch_size_desc"] = "Número de registros a inserir por consulta (1 - 300)";
